from flask import Flask, request, jsonify
from prometheus_flask_exporter import PrometheusMetrics
from prometheus_client import Counter
import psycopg2
import requests
import os
from datetime import datetime, timedelta

app = Flask(__name__)
metrics = PrometheusMetrics(app)

total_requests = Counter('total_requests', 'Total number of requests')
success_requests = Counter('success_requests', 'Number of successful requests')
error_requests = Counter('error_requests', 'Number of error requests')
cache_hit_counter = Counter('cache_hits', 'Number of cache hits')
cache_miss_counter = Counter('cache_misses', 'Number of cache misses')

DB_HOST = os.getenv('DB_HOST', 'localhost')
DB_PORT = int(os.getenv('DB_PORT', 5432))
DB_NAME = os.getenv('DB_NAME', 'postgres')
DB_USER = os.getenv('DB_USER', 'postgres')
DB_PASS = os.getenv('DB_PASS', 'behzad')
GEOAPIFY_API_KEY = os.getenv('GEOAPIFY_API_KEY', '8bf5fbe1a02e4906a5caaf83cb9eb9ea')

def create_database_and_table():
    """Create the database and table if they do not exist."""
    try:
        conn = psycopg2.connect(host=DB_HOST, port=DB_PORT, user=DB_USER, password=DB_PASS)
        conn.autocommit = True
        cur = conn.cursor()

        cur.execute(f"SELECT 1 FROM pg_database WHERE datname = '{DB_NAME}'")
        if not cur.fetchone():
            cur.execute(f"CREATE DATABASE {DB_NAME}")
        
        cur.close()
        conn.close()

        conn = psycopg2.connect(host=DB_HOST, port=DB_PORT, database=DB_NAME, user=DB_USER, password=DB_PASS)
        cur = conn.cursor()
        cur.execute("""
            CREATE TABLE IF NOT EXISTS ip_country (
                id SERIAL PRIMARY KEY,
                ip VARCHAR(15) NOT NULL UNIQUE,
                country VARCHAR(100),
                expiration_time TIMESTAMP NOT NULL
            )
        """)
        conn.commit()
        cur.close()
        conn.close()
    except Exception as e:
        print(f"Error creating database and table: {e}")

create_database_and_table()

conn = psycopg2.connect(host=DB_HOST, port=DB_PORT, database=DB_NAME, user=DB_USER, password=DB_PASS)

@app.route('/get_country', methods=['GET'])
@metrics.do_not_track()
def get_country():
    ip = request.args.get('ip')
    total_requests.inc()  
    if not ip:
        error_requests.inc()  
        return jsonify({'error': 'IP address is required'}), 400

    try:
        cur = conn.cursor()
        cur.execute("SELECT country, expiration_time FROM ip_country WHERE ip = %s", (ip,))
        result = cur.fetchone()

        if result and result[1] > datetime.utcnow():
            cache_hit_counter.inc() 
            success_requests.inc()
            return jsonify({'ip': ip, 'country': result[0], 'cache': 'hit'})

        if result:
            cur.execute("DELETE FROM ip_country WHERE ip = %s", (ip,))

        response = requests.get(f'https://api.geoapify.com/v1/ipinfo?ip={ip}&apiKey={GEOAPIFY_API_KEY}')
        
        if response.status_code != 200:
            raise Exception('Could not reach external API')

        data = response.json()
        country = data.get('country', {}).get('name')

        if not country:
            raise Exception('Could not determine country')

        expiration_time = datetime.utcnow() + timedelta(seconds=30)
        cur.execute("INSERT INTO ip_country (ip, country, expiration_time) VALUES (%s, %s, %s)", (ip, country, expiration_time))
        conn.commit()
        cur.close()

        cache_miss_counter.inc()  
        success_requests.inc()
        return jsonify({'ip': ip, 'country': country, 'cache': 'miss'})

    except Exception as e:
        error_requests.inc()
        return jsonify({'error': str(e)}), 500

@app.route('/health', methods=['GET'])
def health():
    return jsonify({'status': 'healthy'}), 200

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)


