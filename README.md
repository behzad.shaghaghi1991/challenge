**Introduction**

This project outlines the necessary steps to implement and manage a Kubernetes cluster. The aim is to set up a comprehensive monitoring system for the cluster, deploy a PostgreSQL database, develop and deploy a web API using Python, and configure a CI/CD system for the application.

**Project Objectives:**

1.	Kubernetes Cluster Setup: Create a three-node cluster using Ansible.
2.	Monitoring and Alerting: Implement monitoring with Prometheus and Grafana, and configure alerting.
3.	Postgres Database Deployment: Set up and manage a PostgreSQL cluster on Kubernetes.
4.	Web API Development: Develop an API to retrieve country information for a given IP from an external service and store it in the database.
5.	Deployment and CI/CD: Configure and implement a CI/CD process for automatic deployment of the web API on Kubernetes.

**Project Prerequisites**

- **Software and Tools:**

1.	Operating System:
o	Ubuntu 22.04
2.	Infrastructure Management Tools:
o	Ansible: Automate the setup and management of the cluster
o	kubectl: Manage the Kubernetes cluster
o	Helm: Deploy and manage Kubernetes charts
3.	Monitoring and Alerting Tools:
o	Prometheus: Collect and store metrics
o	Grafana: Create graphical dashboards
o	Alertmanager: Manage alerts
o	Slack: Receive alerts
4.	CI/CD Tools:
o	GitLab CI/CD: Automate build and deployment processes
5.	Development and Testing Tools:
o	Docker: Create and manage containers
o	Python and Flask: Develop the web API
o	PostgreSQL: Database

- **Hardware Requirements:**

1.	Server Systems:
o	Three servers (or virtual machines) to set up the Kubernetes cluster
o	At least 2 CPUs and 4 GB RAM for each node

- **Network Requirements:**

1.	Stable Network Connection: Servers must have access to each other and the internet.
2.	DNS Settings: For accessing external services and APIs.
Account and Permission Requirements:
1.	Docker Hub Account: Store and manage Docker images
2.	GitLab Account: Manage code repository and CI/CD setup
3.	Slack Account: Receive alerts
4.	Server Access Permissions: SSH and sudo access to run Ansible

**Steps for Setting Up the Cluster with Ansible**

**Playbook k8s.yaml**

This Playbook helps you install and configure a Kubernetes cluster on Ubuntu servers using Ansible. It consists of three main roles: k8s-install, k8s-master-config, and k8s-workers-config.

- **Role k8s-install:**

1.	Check if /etc/resolv.conf is a symlink: Ensure that the resolv.conf file is properly configured.
2.	Disable UFW firewall: Disable the firewall for the labs.
3.	Disable SWAP: Disable swap to ensure Kubernetes performance.
4.	Add hosts to /etc/hosts: Add all nodes to the hosts file for name resolution.
5.	Load required kernel modules: Ensure required kernel modules are loaded.
6.	Set sysctl parameters: Configure necessary sysctl parameters.
7.	Install dependencies: Install necessary packages and Kubernetes components.
8.	Install Docker: Install Docker for container runtime.
9.	Configure containerd: Set up and configure containerd.

- **Role k8s-master-config:**

1.	Initialize Kubernetes Master Node: Initialize the Kubernetes master node.
2.	Configure kubeconfig for the current user: Set up kubeconfig for the master node.
3.	Install CNI network: Install Calico for networking.
4.	Generate join command: Create a join command for worker nodes.

- **Role k8s-workers-config:**

1.	Copy join command to worker nodes: Transfer the join command to worker nodes.
2.	Execute join command: Join the worker nodes to the Kubernetes cluster.

**Playbook Execution**

To execute the playbook and set up the cluster, use the following command:

`ansible-playbook k8s.yaml -i inventories/hosts`

**Note**: Ensure the user executing the Ansible playbooks has root access on the target servers. This is crucial for performing system-level changes and installations.

This setup ensures that the Kubernetes cluster is correctly installed and configured, with the worker nodes properly joined to the master node.

**Web API**

- **Overview**

This Flask application provides an API to get the country information of an IP address. It uses PostgreSQL to store IP-country mappings and caches results for subsequent requests to improve performance.


- **Prerequisites**


1. Python 3.9+ 
2. PostgreSQL Database: Ensure the database is set up and accessible. 
3. Geoapify API Key: Obtain an API key from [Geoapify](https://www.geoapify.com/). 
4. Docker: Ensure Docker is installed for containerization. 
5. Kubernetes Cluster: Ensure a Kubernetes cluster is set up if deploying using Helm.



- **Environment Variables**

Before running the program, 
You must set the following variables in app or in docker-compose.yaml, otherwise the defaults will be used:

- DB_HOST: Hostname of the PostgreSQL database.
- DB_PORT: Port number of the PostgreSQL database.
- DB_NAME: Name of the PostgreSQL database.
- DB_USER: Username for the PostgreSQL database.
- DB_PASS: Password for the PostgreSQL database.
- GEOAPIFY_API_KEY: API key for accessing Geoapify services.

- **Setting Up the Database**

The application will automatically create the required database and tables if they do not exist. Ensure the PostgreSQL server is running and accessible with the provided credentials.

- **Application Endpoints**

/get_country


- Method: GET

- Description: Retrieves the country information for a given IP address.

- Parameters:
  
   - ip: The IP address to look up.

- Response: JSON object with IP, country, and cache status (hit or miss).

/health


- Method: GET

- Description: Health check endpoint to verify if the application is running.

- Response: JSON object with the status of the application.

**Prometheus Metrics**

The application exposes various metrics for monitoring:

- total_requests: Total number of requests.

- success_requests: Number of successful requests.

- error_requests: Number of error requests.

- cache_hits: Number of cache hits.
- cache_misses: Number of cache misses.

Running the Application
1.	Clone the Repository:

```
git clone https://gitlab.com/behzad.shaghaghi1991/challenge.git
```
```
cd challenge/app-Geolocation
```

2.	Set Environment Variables:

```
DB_HOST=localhost
DB_PORT=5432
DB_NAME=postgres
DB_USER=postgres
DB_PASS=yourpassword
GEOAPIFY_API_KEY=yourapikey
```

3.	Install Dependencies:

```
pip install -r requirements.txt
```

4.	Run the Application:

```
flask run --host=0.0.0.0 --port=5000
```

Docker Deployment

1.	Build Docker Image:

```
docker build -t behzad1991/ip-geolocation:latest -f app-Geolocation/Dockerfile .
```

2.	Run Docker Container:

```
docker run -d -p 5000:5000  behzad1991/ip-geolocation:latest
```

Kubernetes Deployment

1. Deploy using Helm:
```
helm install  ip-geolocation ./helms/charts/ipgeolocation --set-string image.tag=latest
```

**CI/CD Configuration**

The GitLab CI/CD pipeline automates the build and deployment of the application:


- **Variables**
1. IMAGE_NAME: Docker image name.
2. IMAGE_TAG: Tag for the Docker image.
3. KUBE_CONTEXT: Kubernetes context for deployment.
4. REGISTRY_PASS: Docker registry password.
5. REGISTRY_USER: Docker registry username.

**Stages**

1.	Build: Builds the Docker image and pushes it to the Docker registry.
2.	Deploy: Deploys the application to the Kubernetes cluster using Helm.

**Pipeline Execution**

To execute the pipeline:

1.	Commit and push changes to the repository.
2.	Ensure the CI/CD pipeline is triggered on tag creation or manual trigger.

**Setting Up kuber-agent**

The kuber-agent is essential for integrating GitLab with your Kubernetes cluster, enabling automated deployments and management. For detailed setup instructions, refer to the [GitLab documentation](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html). This setup allows for secure and efficient communication between GitLab and your Kubernetes cluster, streamlining the deployment process.

**Deploying kube-prometheus-stack with Ansible**

This guide details the steps to deploy the kube-prometheus-stack Helm chart using Ansible. [This chart](https://artifacthub.io/packages/helm/prometheus-community/kube-prometheus-stack) provides monitoring capabilities for Kubernetes clusters and includes Prometheus, Grafana, and Alertmanager.

**Prerequisites**

1. Kubernetes Cluster: Ensure you have a running Kubernetes cluster.
2. Ansible Setup: Ensure Ansible is installed and configured to connect to your Kubernetes nodes.
3. Helm Installed: Helm must be installed on the system running Ansible.
4. kubectl Installed: kubectl must be installed on the system 6. running Ansible.
5. kubeconfig: The Kubernetes configuration file (kubeconfig) should be accessible on the system running Ansible.
6. Ansible Kubernetes Collection: Install the kubernetes.core collection using:
```
ansible-galaxy collection install kubernetes.core
```
**Steps**


- **Set Kubeconfig Path:**

Define the path to the kubeconfig file, which is necessary for Helm and kubectl to communicate with the Kubernetes cluster.

Path on the source machine: /home/ubuntu/.kube/config


- **Create Directory for Helm Chart on Remote Server:**

Create a directory on the remote server where the Helm chart files will be stored. This step ensures that the necessary files are organized and accessible for deployment.

Directory on the remote server: /tmp/helm_chart

- **Copy Helm Chart to Remote Server:**

Transfer the Helm chart files from your local machine to the remote server. This step involves copying the entire Helm chart directory to a specified location on the remote server.

Source directory on the local machine: /files/kube-prometheus-stack

Destination directory on the remote server: /tmp/helm_chart

- **Deploy Helm Chart:**

Use the Helm module in Ansible to deploy the kube-prometheus-stack chart from the copied files on the remote server. This step involves specifying the chart reference and the Kubernetes namespace for the release.

**Execution**

To execute the playbook, use the following command:

```
ansible-playbook install-helms.yaml -i inventories/hosts
```


**Deploying PostgreSQL Helm Chart with Ansible**

**Execution**

To execute the playbook, use the following command:

```
ansible-playbook install-helms.yaml -i inventories/hosts

```

**Deploying Ingress-NGINX Helm Chart with Ansible**

**Execution**

```
ansible-playbook install-helms.yaml -i inventories/hosts

```


**Note:**

Executing the following command:

```
ansible-playbook install-helms.yaml -i inventories/hosts

```

will install Ingress-NGINX, PostgreSQL, kube-prometheus-stack, and app-Geolocation using Helm charts.




**Ingress Controller Helm Chart**

**Network Configuration**

•	Host Network: Enabled (true), allowing the controller pods to use the host’s network namespace.

- Ports:
 
  - HTTP: Port 80
  - HTTPS: Port 443
- Host Ports: Enabled, mapping:
  
  - HTTP: Port 80
  - HTTPS: Port 443

- Service Configuration
- Enabled: Yes
- Type: NodePort
- Ports:
   - HTTP: Port 80
   - HTTPS: Port 443

**Instructions**

1.	Network Configuration:
- Verify that the host network is enabled and the correct ports are configured for HTTP and HTTPS.
- Adjust DNS policy and config if necessary.
2.	Service Configuration:
- Ensure the service type is set to NodePort.
- Confirm the ports for HTTP and HTTPS are correctly mapped.
- Customize session affinity and external traffic policy based on your requirements.



**IP-Geolocation Helm Chart Configuration Details**

**Image Configuration**

- Registry: docker.io
- Repository: behzad1991/ip-geolocation
- Pull Policy: Always


**Database Configuration**

These credentials must match your PostgreSQL setup.
- Username: postgres
- Password: behzad
- Host: postgresql-primary
- Port: 5432
- atabase Name: postgres

**Service Configuration**

- Service Type: ClusterIP
- Service Port: 5000
- Annotations:
  - prometheus.io/scrape: 'true'
  - prometheus.io/path: /metrics
  - prometheus.io/port: '5000'

Prometheus annotations are included to monitor the application's metrics.

**Ingress Configuration**
- Enabled: true
- Class Name: nginx
- Annotations:
    - spec.ingressClassName: nginx
- Hosts:
- Host: behzad.ir
- Paths:
  - Path: /get_country
  - Path Type: Prefix


The application will be accessible at http://behzad.ir/get_country.


**Important Configuration Details**


- Service Accessibility: 

The application will be accessible at http://behzad.ir/get_country after the Helm chart is deployed.


- Service Address: 

Ensure the behzad.ir domain points to the IP address 5.34.196.87, which should be one of your worker nodes.

Post-Deployment Steps

1.	Update /etc/hosts:
- Add the following line to your /etc/hosts file:

`5.34.196.87 behzad.ir`

- This maps behzad.ir to the worker node's IP address.

2. Verify Application Accessibility:

Use curl to test the deployment by sending a request to the application:

`curl "http://behzad.ir/get_country?ip=185.143.233.201"`

This command checks the location information for the IP address 185.143.233.201.




**Important Considerations for Using Helm Chart kube-prometheus-stack**

When deploying the kube-prometheus-stack Helm chart, keep the following considerations in mind:

**Grafana User and Password**

It is crucial to set a secure password for the Grafana admin user in the values.yaml file.

**Service Exposure**

If you intend to access Prometheus, Grafana, and Alertmanager services externally, set their service.type to NodePort in the values.yaml file. This will expose the services outside of the Kubernetes cluster.

**Slack Integration for Alerts**

Configure the Slack webhook URL and specify the channel for alert notifications in the values.yaml file under the Alertmanager configuration. This ensures that alerts are sent to your desired Slack channel.


**Important Considerations for Using Helm Chart postgresql**

When deploying the postgresql Helm chart, keep the following considerations in mind:

**User and Password Configuration**

Replication User:
 Create a user with replication access during the installation of the Helm chart.

Root Password: 

Set a password for the user with root access.

**Secret Configuration**

Secret File: Use the secret.yaml file provided in the project to create a Kubernetes Secret that includes the necessary passwords for accessing PostgreSQL.
Apply the secret.yaml file to create the Kubernetes Secret that includes the PostgreSQL passwords:
```
kubectl apply -f secret.yaml
```

**Persistent Storage**

Persistent Volumes and Claims: 

Use the files located in the storages-files directory to create PersistentVolumes (PV) and PersistentVolumeClaims (PVC) required for the PostgreSQL database.
By following these guidelines, you can ensure that your PostgreSQL deployment is secure, properly configured, and has the necessary persistent storage setup.

Use the following commands to create the necessary PVs and PVCs for PostgreSQL storage:

```
kubectl apply -f pv.yaml
kubectl apply -f pvc.yaml
kubectl apply -f pvcrep.yaml
kubectl apply -f pvrep.yaml
```



